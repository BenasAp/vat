# vat-app

Api for countries: [https://restcountries.eu/](https://restcountries.eu/)

Api for VAT: [https://vatlayer.com/](https://vatlayer.com/)

Components lib: [https://vuetifyjs.com/](https://vuetifyjs.com/)

Vat layer has free plan and only 100 requests I used 52 of them for testing 

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
