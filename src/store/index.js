import Vue from "vue";
import Vuex from "vuex";
import * as countries from "@/store/modules/countries";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    countries,
  },
});
