import axios from "axios";

const apiKey = "19fce88a5ab25d0812f44e7d659d3329";

export const getVatByCountry = (code) =>
  axios.get(
    `http://apilayer.net/api/rate?access_key=${apiKey}&country_code=${code}`
  );
