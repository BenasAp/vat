import CountriesService from "@/services/CountriesService";

export const namespaced = true;

export const state = {
  isLoading: false,
  countries: [],
  errors: null,
};

export const mutations = {
  SET_IS_LOADING: (state, payload) => (state.isLoading = payload),
  SET_COUNTRIES: (state, payload) => (state.countries = payload),
  SET_ERRORS: (state, payload) => (state.errors = payload),
};

export const getters = {
  mapedCountries(state) {
    return state.countries.map((countrie) => ({
      name: countrie.name,
      value: [
        {
          code: countrie.alpha2Code,
          region: countrie.region,
          name: countrie.name,
        },
      ],
    }));
  },
};

export const actions = {
  async fetchCountries({ commit }) {
    commit("SET_IS_LOADING", true);

    try {
      const { data } = await CountriesService.getCounties();

      commit("SET_IS_LOADING", false);
      commit("SET_COUNTRIES", data);
    } catch (e) {
      commit("SET_IS_LOADING", false);
      commit("SET_ERRORS", e);
    }
  },
};
