import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://restcountries.eu/rest/v2/",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  getCounties() {
    return apiClient.get("all");
  },
};
